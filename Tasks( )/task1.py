# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'task.ui'
#
# Created by: PyQt5 UI code generator 5.9
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

from status import status

class Ui_MindMainWindow(object):
    def setupUi(self, MindMainWindow):
        self._MindMainWindow = MindMainWindow
        self.MindMainWindow()
        def MindMainWindow(self):
            self._MindMainWindow.setObjectName("MindMainWindow")
            self._MindMainWindow.resize(870, 370)
            self._MindMainWindow.setStyleSheet("")

        def centralWidget(self):
            self.centralWidget = QtWidgets.QWidget(self._MindMainWindow)
            self.centralWidget.setStyleSheet("background-color : #020805;")
            self.centralWidget.setObjectName("centralWidget")

        def tophbox(self):
            self.tophboxWidget = QtWidgets.QWidget(self.centralWidget)
            self.tophboxWidget.setGeometry(QtCore.QRect(10, 20, 851, 33))
            self.tophboxWidget.setObjectName("tophboxWidget")
            self.tophbox = QtWidgets.QHBoxLayout(self.tophboxWidget)
            self.tophbox.setSizeConstraint(QtWidgets.QLayout.SetNoConstraint)
            self.tophbox.setContentsMargins(20, 11, 20, 0)
            self.tophbox.setSpacing(150)
            self.tophbox.setObjectName("tophbox")

        self.status = status("status" , self.tophboxWidget)

        self.tophbox.addWidget(self.status)

        self.header = QtWidgets.QPushButton(self.tophboxWidget)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(18)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.header.setFont(font)
        self.header.setAutoFillBackground(False)
        self.header.setStyleSheet("border:2px solid;\n"
"border-color: rgb(0, 0, 255);")
        self.header.setObjectName("header")
        self.tophbox.addWidget(self.header)
        self.user = QtWidgets.QPushButton(self.tophboxWidget)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(20)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.user.setFont(font)
        self.user.setAutoFillBackground(False)
        self.user.setStyleSheet("border:3px solid;\n"
"border-color: rgb(0, 0, 255);")
        self.user.setObjectName("user")
        self.tophbox.addWidget(self.user)
        self.tophbox.setStretch(0, 1)
        self.tophbox.setStretch(1, 1)
        self.tophbox.setStretch(2, 1)
        self.progressBar = QtWidgets.QProgressBar(self.centralWidget)
        self.progressBar.setGeometry(QtCore.QRect(30, 60, 811, 16))
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Window, brush)
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Button, brush)
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Base, brush)
        brush = QtGui.QBrush(QtGui.QColor(2, 8, 5))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Window, brush)
        self.progressBar.setPalette(palette)
        self.progressBar.setAutoFillBackground(False)
        self.progressBar.setStyleSheet("")
        self.progressBar.setProperty("value", 24)
        self.progressBar.setTextVisible(False)
        self.progressBar.setObjectName("progressBar")
        self.tophboxWidget_2 = QtWidgets.QWidget(self.centralWidget)
        self.tophboxWidget_2.setGeometry(QtCore.QRect(10, 140, 851, 41))
        self.tophboxWidget_2.setObjectName("tophboxWidget_2")
        self.tophbox_2 = QtWidgets.QHBoxLayout(self.tophboxWidget_2)
        self.tophbox_2.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.tophbox_2.setContentsMargins(10, 11, 10, 11)
        self.tophbox_2.setSpacing(13)
        self.tophbox_2.setObjectName("tophbox_2")
        self.status_4 = QtWidgets.QPushButton(self.tophboxWidget_2)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.status_4.setFont(font)
        self.status_4.setAutoFillBackground(False)
        self.status_4.setStyleSheet("border:2px solid;\n"
"border-color: rgb(0, 0, 255);")
        self.status_4.setObjectName("status_4")
        self.tophbox_2.addWidget(self.status_4)
        self.label_3 = QtWidgets.QLabel(self.tophboxWidget_2)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(14)
        self.label_3.setFont(font)
        self.label_3.setObjectName("label_3")
        self.tophbox_2.addWidget(self.label_3)
        self.lineEdit = QtWidgets.QLineEdit(self.tophboxWidget_2)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(14)
        self.lineEdit.setFont(font)
        self.lineEdit.setObjectName("lineEdit")
        self.tophbox_2.addWidget(self.lineEdit)
        self.tophboxWidget_3 = QtWidgets.QWidget(self.centralWidget)
        self.tophboxWidget_3.setGeometry(QtCore.QRect(10, 190, 851, 41))
        self.tophboxWidget_3.setObjectName("tophboxWidget_3")
        self.tophbox_3 = QtWidgets.QHBoxLayout(self.tophboxWidget_3)
        self.tophbox_3.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.tophbox_3.setContentsMargins(10, 11, 10, 11)
        self.tophbox_3.setSpacing(13)
        self.tophbox_3.setObjectName("tophbox_3")
        self.status_5 = QtWidgets.QPushButton(self.tophboxWidget_3)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.status_5.setFont(font)
        self.status_5.setAutoFillBackground(False)
        self.status_5.setStyleSheet("border:2px solid;\n"
"border-color: rgb(255,0, 0);")
        self.status_5.setObjectName("status_5")
        self.tophbox_3.addWidget(self.status_5)
        self.label_4 = QtWidgets.QLabel(self.tophboxWidget_3)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(14)
        self.label_4.setFont(font)
        self.label_4.setObjectName("label_4")
        self.tophbox_3.addWidget(self.label_4)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.tophboxWidget_3)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(14)
        self.lineEdit_2.setFont(font)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.tophbox_3.addWidget(self.lineEdit_2)
        self.tophboxWidget_4 = QtWidgets.QWidget(self.centralWidget)
        self.tophboxWidget_4.setGeometry(QtCore.QRect(10, 90, 851, 41))
        self.tophboxWidget_4.setObjectName("tophboxWidget_4")
        self.tophbox_4 = QtWidgets.QHBoxLayout(self.tophboxWidget_4)
        self.tophbox_4.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.tophbox_4.setContentsMargins(10, 11, 10, 11)
        self.tophbox_4.setSpacing(13)
        self.tophbox_4.setObjectName("tophbox_4")
        self.status_6 = QtWidgets.QPushButton(self.tophboxWidget_4)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(16)
        font.setBold(False)
        font.setItalic(False)
        font.setUnderline(False)
        font.setWeight(50)
        font.setStrikeOut(False)
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.status_6.setFont(font)
        self.status_6.setAutoFillBackground(False)
        self.status_6.setStyleSheet("border:2px solid;\n"
"border-color: rgb(0, 255, 0);")
        self.status_6.setObjectName("status_6")
        self.tophbox_4.addWidget(self.status_6)
        self.label_5 = QtWidgets.QLabel(self.tophboxWidget_4)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(14)
        self.label_5.setFont(font)
        self.label_5.setObjectName("label_5")
        self.tophbox_4.addWidget(self.label_5)
        self.lineEdit_3 = QtWidgets.QLineEdit(self.tophboxWidget_4)
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(14)
        self.lineEdit_3.setFont(font)
        self.lineEdit_3.setObjectName("lineEdit_3")
        self.tophbox_4.addWidget(self.lineEdit_3)
        self.label = QtWidgets.QLabel(self.centralWidget)
        self.label.setGeometry(QtCore.QRect(10, 240, 201, 51))
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(20)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.label.setFont(font)
        self.label.setAutoFillBackground(False)
        self.label.setStyleSheet("")
        self.label.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label.setLineWidth(0)
        self.label.setTextFormat(QtCore.Qt.PlainText)
        self.label.setScaledContents(False)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setWordWrap(False)
        self.label.setIndent(0)
        self.label.setObjectName("label")
        self.label_2 = QtWidgets.QLabel(self.centralWidget)
        self.label_2.setGeometry(QtCore.QRect(20, 310, 201, 51))
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(20)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.label_2.setFont(font)
        self.label_2.setAutoFillBackground(False)
        self.label_2.setStyleSheet("")
        self.label_2.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_2.setLineWidth(0)
        self.label_2.setTextFormat(QtCore.Qt.PlainText)
        self.label_2.setScaledContents(False)
        self.label_2.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        self.label_2.setWordWrap(False)
        self.label_2.setIndent(0)
        self.label_2.setObjectName("label_2")
        self.label_6 = QtWidgets.QLabel(self.centralWidget)
        self.label_6.setGeometry(QtCore.QRect(280, 260, 251, 81))
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(24)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.label_6.setFont(font)
        self.label_6.setAutoFillBackground(False)
        self.label_6.setStyleSheet("")
        self.label_6.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_6.setLineWidth(0)
        self.label_6.setTextFormat(QtCore.Qt.PlainText)
        self.label_6.setScaledContents(False)
        self.label_6.setAlignment(QtCore.Qt.AlignCenter)
        self.label_6.setWordWrap(False)
        self.label_6.setIndent(0)
        self.label_6.setObjectName("label_6")
        self.label_7 = QtWidgets.QLabel(self.centralWidget)
        self.label_7.setGeometry(QtCore.QRect(580, 260, 251, 81))
        font = QtGui.QFont()
        font.setFamily("Aldrich")
        font.setPointSize(24)
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.label_7.setFont(font)
        self.label_7.setAutoFillBackground(False)
        self.label_7.setStyleSheet("")
        self.label_7.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.label_7.setLineWidth(0)
        self.label_7.setTextFormat(QtCore.Qt.PlainText)
        self.label_7.setScaledContents(False)
        self.label_7.setAlignment(QtCore.Qt.AlignCenter)
        self.label_7.setWordWrap(False)
        self.label_7.setIndent(0)
        self.label_7.setObjectName("label_7")
        self._MindMainWindow.setCentralWidget(self.centralWidget)

        self.retranslateUi(self._MindMainWindow)
        QtCore.QMetaObject.connectSlotsByName(self._MindMainWindow)

    def retranslateUi(self, MindMainWindow):
        _translate = QtCore.QCoreApplication.translate
        self._MindMainWindow.setWindowTitle(_translate("MindMainWindow", "MindMainWindow"))
        self.status.setText(_translate("MindMainWindow", "user"))
        self.header.setText(_translate("MindMainWindow", "header"))
        self.user.setText(_translate("MindMainWindow", "status"))
        self.status_4.setText(_translate("MindMainWindow", "  >  "))
        self.label_3.setText(_translate("MindMainWindow", "170"))
        self.status_5.setText(_translate("MindMainWindow", " - "))
        self.label_4.setText(_translate("MindMainWindow", "170"))
        self.status_6.setText(_translate("MindMainWindow", " + "))
        self.label_5.setText(_translate("MindMainWindow", "170"))
        self.label.setText(_translate("MindMainWindow", "UNMODIFIED"))
        self.label_2.setText(_translate("MindMainWindow", "22:35:58"))
        self.label_6.setText(_translate("MindMainWindow", "36.60M"))
        self.label_7.setText(_translate("MindMainWindow", "6%"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MindMainWindow = QtWidgets.QMainWindow()
    ui = Ui_MindMainWindow()
    ui.setupUi(MindMainWindow)
    MindMainWindow.show()
    sys.exit(app.exec_())

