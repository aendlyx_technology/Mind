def declaringQWidgets(self):
	print(f"Declaring QWidgets")
 	self._main = {}

 	self._main['user'] 			= QPushButton( 	"user" 			,	parent )

 	self._main['header'] 		= QPushButton( 	"header" 		,	parent )

 	self._main['status'] 		= QPushButton( 	"status" 		,	parent )

 	self._main['progress'] 		= QProgressBar(				  		parent )

 	self._main['btnNext'] 		= QPushButton( 	"btnNext" 		, 	parent )
 	self._main['numNext'] 		= QLabel( 		"numNext" 		, 	parent )
 	self._main['textNext']		= QLineEdit( 	"textNext" 		, 	parent )

 	self._main['btnActual'] 	= QPushButton( 	"btnActual"		, 	parent )
 	self._main['numActual'] 	= QLabel( 		"numActual" 	, 	parent )
 	self._main['textActual'] 	= QLineEdit( 	"textActual" 	, 	parent )

 	self._main['btnPrevious'] 	= QPushButton( 	"btnPrevious"	, 	parent )
 	self._main['numPrevious'] 	= QLabel( 		"numPrevious" 	, 	parent )
 	self._main['textPrevious'] 	= QLineEdit( 	"textPrevious" 	, 	parent )
 	print(f"self._main")
 	print("____        ____        ____        ____        ____        ____")
 	
