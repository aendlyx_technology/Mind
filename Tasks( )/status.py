from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *

class status(QPushButton):

        mouseHover = pyqtSignal(bool)

        def __init__(self, text, parent=None):

                QPushButton.__init__(self, text, parent)

                self.font = QFont()
                self.font.setFamily("Aldrich")
                self.font.setPointSize(16)
                self.font.setBold(False)
                self.font.setItalic(False)
                self.font.setUnderline(False)
                self.font.setWeight(50)
                self.font.setStrikeOut(False)
                self.font.setKerning(True)
                self.font.setStyleStrategy(QFont.PreferAntialias)

                self.setFont(self.font)
                self.setAutoFillBackground(False)
                self.setStyleSheet("QPushButton { border:1px solid; border-color: rgb(0, 0, 255); color:white;}")
                self.setObjectName("status")

