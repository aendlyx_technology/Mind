# The purpose of this file is to provide a graphical interface to create users









#________________________________________________________________________________________________________________________________________________#
# - - - - - - - - - #

# GUI Mind Users 1.0

# - - - - - - - - - #
from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Mind_Users(object):
	def setupUi(self, MindUsersWindow):
		self._MindUsersWindow = MindUsersWindow
		self.MindUsersWindow()
	def MindUsersWindow(self):
		self._MindUsersWindow.setObjectName("MindUsersWindow")
		self._MindUsersWindow.setWindowTitle("Mind_Users")
		self._MindUsersWindow.resize(870, 370)
		self._MindUsersWindow.setStyleSheet("")
#________________________________________________________________________________________________________________________________________________#









#________________________________________________________________________________________________________________________________________________#
# - - - #

# MAIN

# - - - #
import sys
if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MindUsersWindow = QtWidgets.QMainWindow()
    ui = Ui_Mind_Users()
    ui.setupUi(MindUsersWindow)
    MindUsersWindow.show()
    sys.exit(app.exec_())
#________________________________________________________________________________________________________________________________________________#